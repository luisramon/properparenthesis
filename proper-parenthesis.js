module.exports = properParenthesis;

const OPENS = "([{";
const CLOSERS = ")]}";

function checkBalanceSymbols(input = '') {
     let stack = [],
        isBalance = true,
        index = 0,
        length = input.length,
        result = 'NO';
    
    while (index < length && isBalance) {
        let symbol = input[index];
        if (OPENS.indexOf(symbol) !== -1) {
            stack.push(symbol);
        } else if (CLOSERS.indexOf(symbol) !== -1) {
            if (stack.length === 0) {
                isBalance = false;
            } else {
                let top = stack.pop();
                if (!match(top, symbol)) {
                    balance = false;
                }
            }
        }
        index++;
    }

    if (isBalance === true && stack.length === 0 && input !== '') {
        result = 'YES';
    }

    return result;
}

function match(open, close) {
    return OPENS.indexOf(open) === CLOSERS.indexOf(close);
}

/**
 * Return an array with YES's and No depending if the parentheses are balanced.
 */
function properParenthesis(input) {
    return input.map(checkBalanceSymbols);
}