var properParenthesis = require('./proper-parenthesis');
var assert = require('chai').assert;

describe('Proper Parenthesis', function() {
    it("Should iter the array and return another array fill in with YES's and No's", function() {
        var expected = ['YES', 'NO', 'YES', 'NO', 'NO', 'NO'];
        var input = [
            '() [] () ([]()[])',
            '( (] ([)]',
            '{}[]()',
            '{{}]}',
            '{[}',
            '[}]'
        ];

        var result = properParenthesis(input);

        assert.deepEqual(result, expected);
    });

    it("Should return an array with only NO's", function() {
        var expected = ['NO', 'NO', 'NO'];
        var input = ['([[[])', '{{( )} }]', ''];

        var result = properParenthesis(input);

        assert.deepEqual(result, expected);
    });

    it('Should return an empty array', function() {
        var expected = [];
        var input = [];

        var result = properParenthesis(input);

        assert.deepEqual(result, expected);
    });
});