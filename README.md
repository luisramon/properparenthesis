# Proper Parenthesis

Implement a function which prints 'YES' into output if the string has a proper amount of parenthesis.
By a proper amount of parenthesis we understand the case when for each opening parentheses we have appropriated closing one.

    var input = [
        '() [] () ([]()[])',
        '( (] ([)]',
        '{}[]()',
        '{{}]}',
        '{[}',
        '[}]'
    ];

    var result = properParenthesis(input);
    // result should be equals to
    // ['YES', 'NO', 'YES', 'NO', 'NO', 'NO']
